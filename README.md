
# Red Hat Partner Bootcamp
Slidedecks for the enablement Session

## Day1 Introduction

 - **How to work with Red Hat** - [PDF](How to work with Red Hat.pdf)
 - **Red Hat Culture** - [PDF](Introducing_Open_Organizational_Culture.pdf)
 - **Hybrid Cloud Strategy** - [PDF](Fr_P1_Why Hybrid Cloud.pdf) 

## Day 2 RHEL & Ansible
 - **Ansible Automation Platform 2** - [PDF](Ansible Automation Platform 2.pdf)
 - **SAP Automation with Ansible** - [PDF](SAP-Automation.pdf)

## Day 3 Openshift Infrastructure
  - **Openshift Innovation** - [PDF](Wed_P1_Red Hat OpenShift -- Innovation without limitation.pdf)
  - **Openshift Architecture** - [PDF](Wed_P2_OpenShift 4 Architecture.pdf)
  - **Openshift Installation** - [PDF](Wed_P3_OCP_Install.pdf)
  - **Openshift Plus** - [PDF](Wed_P4_OpenShift Plus.pdf)
  - **How to run a great Demo** - [PDF](Alfreds-Introduction into story telling.pdf)

## Day 4 Cloud Native, AI & Edge

  - **OpenShift for Developers** - [PDF](Thu_OpenShift_For_Devs.pdf)
  - **Introduction to CloudNative** - [PDF](Thu_CloudNative.pdf)
  - **OpenShift Data Science** - [PDF](Openshift Data Science Introduction.pdf)
  - **Developer Adoption Program** - [PDF](DeveloperAdoptionPartnerBootcamp.pdf)  
  - **Edge Computing** - [PDF](Introduction to Edge Computing.pdf)    

(v1.0 7.7.22)



  


